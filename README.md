mini\_auth
==============

The mini\_auth package provides a very basic authentication
mechanism.

It includes the unittests, integration tests and UI tests required in our Intellisys training.


Dependencies
------------

The following packages are required to run the software:

- cherrypy
- simplejson
- sqlite3
- coverage
- nose

To install dependencies run:

pip install -r requirements.txt   (you may use virtualenv)

pip install https://github.com/passy/nose-lettuce/tarball/master

to run the project use:

sudo python ./mini_auth.py                                (need permissions to run in port 81)

To run tests use:
nosetests ./tests_unittests/ -v --with-coverage     (for unittests)

nosetests ./tests_integration/ -v                   (for integration tests)

nosetests --with-lettuce -s --lettuce-path=tests_ui (for ui tests)