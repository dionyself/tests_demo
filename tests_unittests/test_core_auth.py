import minimock
import unittest
import core.auth
import core.send_email
import core.userdb
import cherrypy
from collections import OrderedDict


class TestAuthMethods(unittest.TestCase):
    """ unittest for tests_demo/mini Auth """
    @classmethod
    def setUpClass(cls):
        cls.template_args = OrderedDict()
        cls.template_args['user_id'] = None,
        cls.template_args['root_url'] = "http://localhost:81/"
        cls.auth_args = OrderedDict()
        cls.auth_args["session_key"] = "test_sessionkey"
        cls.auth_args["template_func"] = lambda *x, **y: (x, y)
        cls.auth_args["template_args"] = cls.template_args
        cls.auth_args["root_url"] = cls.template_args['root_url']
        cls.auth_args["title"] = "test_title"
        cls.auth_args["user_db_f"] = "test_db_file.db"

    def setUp(self):
        minimock.mock('core.userdb.UserDatabase', returns=self.auth_args["user_db_f"])
        minimock.mock('core.send_email.Email', returns="called" )
        self._auth = core.auth.Auth(*self.auth_args.values())

    def tearDown(self):
        minimock.restore()

    def test_init(self):
        """ Test that Auth instance takes correct values """
        self.assertTrue(
            isinstance(self._auth, core.auth.Auth),
            "Created obj in not instance of Auth")
        self.assertEqual(self._auth.title, self.auth_args['title'])
        self.assertEqual(self._auth.root_url, self.auth_args["root_url"])
        self.assertTrue(self._auth.udb == self.auth_args["user_db_f"], "Not DB initializer not called")
        self.assertEqual(self._auth.SESSION_USER_KEY, self.auth_args["session_key"])

    def test_set_email_settings(self):
        """ Testing email setings """
        self._auth.set_email_settings(
            "smtp_server", "port", "fr", "user", "password")
        self.assertTrue(self._auth.email == "called", "Email initializer not called")

    def test_register(self):
        """ Testing Register function """
        self._auth._template_args = minimock.Mock('template_args', returns={'arg1':"arg1_value"})
        action_info = self._auth.register()
        self.assertTrue(action_info[0][0] == 'register.html', "Using Incorrect template filename")
        self.assertTrue(action_info[1]['arg1'] == "arg1_value", "Getting Incorrect argument")

    def test_aregister_success(self):
        """ Testing ajax register function """
        self._auth.udb = minimock.Mock('get_users')
        self._auth.udb.get_users.mock_returns = {}
        self._auth.udb.add_user.mock_returns = "faketoken"
        self._auth._logout = lambda: None
        self._auth.email = minimock.Mock('email')
        self._auth.email.send_email.mock_returns = ""
        result = self._auth.aregister("username", "password")
        self.assertTrue(
            "http://localhost:81/activate_account" in result,
            "Not redirecting to activate page")

    def test_aregister_fails(self):
        """ Testing ajax funtion with arbitrary data """
        # case no email address provided
        assert 'email address' in self._auth.aregister("", "password")

        # case user already exists
        self._auth.udb = minimock.Mock('get_users')
        self._auth.udb.get_users.mock_returns = {"test_user": 1}
        assert 'already exists' in self._auth.aregister("test_user", "password")


if __name__ == '__main__':
    unittest.main()
