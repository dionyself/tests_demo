import minimock
import unittest
import core.auth
import core.send_email
import cherrypy
from collections import OrderedDict


class TestRegisterIntegration(unittest.TestCase):
    """ integration for register """
    @classmethod
    def setUpClass(cls):
        cls.template_args = OrderedDict()
        cls.template_args['user_id'] = None,
        cls.template_args['root_url'] = "http://localhost:81/"
        cls.auth_args = OrderedDict()
        cls.auth_args["session_key"] = "test_sessionkey"
        cls.auth_args["template_func"] = lambda *x, **y: (x, y)
        cls.auth_args["template_args"] = cls.template_args
        cls.auth_args["root_url"] = cls.template_args['root_url']
        cls.auth_args["title"] = "test_title"
        cls.auth_args["user_db_f"] = "test_db_file.db"

    def setUp(self):
        cherrypy.session = {self.auth_args["session_key"]: None}
        minimock.mock('core.send_email.Email', returns="called")
        self._auth = core.auth.Auth(*self.auth_args.values())

    def tearDown(self):
        minimock.restore()

    def test_register(self):
        """ INTEGRATION Testing Register function """
        action_info = self._auth.register()
        self.assertTrue(action_info[0][0] == 'register.html', "Using Incorrect template filename")

    def test_aregister_success(self):
        """ INTEGRATION Testing ajax register function """
        result = self._auth.aregister("hjhk", "password")
        self.assertTrue(
            "http://localhost:81/activate_account" in result,
            "Not redirecting to activate page")

if __name__ == '__main__':
    unittest.main()
