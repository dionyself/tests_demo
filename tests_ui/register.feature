Feature: Register a user
    In order to play with Lettuce
    As beginners
    We'll implement register test

    Scenario: User is going to register
        Given I am in register page
        When I enter "username@test.com"
          and "password"
        Then I should see "registration success"
