from lettuce import before, world, step, after
from nose.tools import assert_equals
from testtools.matchers import Equals
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait


@before.all
def setup_browser():
    _profile = webdriver.FirefoxProfile()
    _profile.native_events_enabled = True
    world.browser = webdriver.Firefox(_profile)


@step('I am in register page')
def navigate_to_register(step):
    print ('Attempting to navigate to register page')
    world.browser.get("http://localhost:81/register")


@step('I enter "(.*)"')
def when_i_enter_name(step, name):
    username_field = world.browser.find_element(By.NAME, "username")
    username_field.send_keys(name)

@step('and "(.*)"')
def when_i_enter_pasword(step, pwd):
    password_field = world.browser.find_element(By.NAME, "password")
    password_field.send_keys(pwd)
    world.browser.find_element(By.CLASS_NAME, "submit").click()
    WebDriverWait(world.browser, 3).until(
        EC.text_to_be_present_in_element((By.ID, "msg"), 'activation url'), "")
    msg = world.browser.find_element(By.ID, "msg").text
    world.result = "registration success" if msg else "Fail"

@step('I should see "(.*)"')
def result(step, expected_result):
    actual_result = world.result
    assert_equals(expected_result, actual_result)

@after.all
def close_driver(driver):
    print driver
    world.browser.close()
